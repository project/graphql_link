<?php

namespace Drupal\graphql_link\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\Core\Url;

/**
 * Schema Extension for link fields.
 *
 * @SchemaExtension(
 *   id = "graphql_link_extension",
 *   name = "GraphQL Link",
 *   description = "Provides a link field extension.",
 *   schema = "graphql_link",
 * )
 */
class GraphQLLinkSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Link', 'linkTitle',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('field_item:link'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('title'))
    );

    $registry->addFieldResolver('Link', 'linkURL',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('field_item:link'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('uri')),
        $builder->callback(function ($uri) {
          return Url::fromUri($uri);
        }),
        $builder->produce('url_path')
          ->map('url', $builder->fromParent())
      )
    );

  }

}
