# GraphQL Link

**Project Status:** Unsupported

This is a small module designed to work with the Drupal Link field type. It provides a GraphQL field type that can be used to return a link object.

## Obsolete Notice

**This module is now considered obsolete and is no longer maintained.**

We strongly recommend that users migrate to alternative solutions as this module will not receive any updates or support. Below are some recommended alternatives:

- [GraphQL Compose](https://www.drupal.org/project/graphql_compose)
- [GraphQL Core Schema](https://www.drupal.org/project/graphql_core_schema)

## Requirements

- [GraphQL](https://www.drupal.org/project/graphql) 4.x

## Usage

This module provides a GraphQL field type that can be used like this:

```graphql
type MyType {
  myField: [Link]
}
```

The field will return a link object with the following properties:

- linkURL: The URL of the link
- linkTitle: The title of the link
